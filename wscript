#!/usr/bin/python3
# encoding: utf-8

# set the font name, version, licensing and description
APPNAME="AndikaMtihani"
familyname = APPNAME
FILENAMEBASE="AndikaMtihani"

# Get VERSION and BUILDLABEL from Regular UFO; must be first function call:
getufoinfo('source/' + familyname + '-Regular' + '.ufo')


DESC_SHORT = "Test font for UFO workflows"
DESC_NAME = "AndikaMtihani"


# building 
for style in ('-Regular','-Bold','-Italic','-BoldItalic') :
    font(target = FILENAMEBASE + style + '.ttf',
        buildusingfontforge = 1,
        source = 'source/' + FILENAMEBASE + style + '.ufo',
        version = VERSION,
        license = ofl('Andika'),
        script = 'latn',
        fret = fret(params = '-r'),
        woff = woff()
    )
